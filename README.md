# My template

## Getting started
Install all dependencies listed in package.json using npm.
```
    npm install --save-dev {dependencies}
```

### Bundle js code and run webpack-dev-server
```
    npm run start
``` 

### Running tests
```
    npm run test 
```
 or 
 ```
    ./node_modules/.bin/nightwatch
```

### Author
* **Sotnichenko Roman** (Telegram: @RomanSo)

### License
This project is licensed under the MIT License.


