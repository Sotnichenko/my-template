module.exports = {
    'Demo test Google': function(client) {
        client
            .url('http://www.google.com')
            .waitForElementVisible('body', 1000)
            .setValue('input[type=text]', 'nigthwatch')
            .waitForElementVisible('button[value="Поиск в Google" class="lsb"]', 1000)
            .click('button[value="Поиск в Google" class="lsb"]')
            .pause()
            .assert.containsText('#main', "Night Watch")
            .end();
    }
};
